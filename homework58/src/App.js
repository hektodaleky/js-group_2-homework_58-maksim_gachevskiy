import React, {Component} from "react";
import "./App.css";
import Modal from "./components/UI/Modal/Modal";

class App extends Component {
    state = {
        show: true
    };
    closed = () => {
        this.setState({show: false});
    };

    render() {
        return (
            <Modal
                show={this.state.show}
                closed={this.closed}
                title="Some kinda modal title"
            >
                <p>This is modal content</p>
            </Modal>

        );
    }
}

export default App;
