import React from "react";
import "./Modal.css";
import Wrapper from "../../../hoc/Wrapper";

const Modal = props => (
    <Wrapper>

        <div className="Modal"  style={{
            transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
            opacity: props.show ? '1' : '0'
        }}
        ><i onClick={props.closed}>x</i><h1>{props.title}</h1>
            {props.children}
        </div>
    </Wrapper>
);
export default Modal;