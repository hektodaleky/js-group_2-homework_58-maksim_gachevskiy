import React, {Component} from "react";
import "./App.css";
import Alert from "./components/UI/Alert";

class App extends Component {
    someHandler = (id) => {
        const index = this.state.components.findIndex(find => find.id === id);
        let tmpList = [...this.state.components];
        tmpList = tmpList.filter(item => item != tmpList[index]);
        this.setState({components: tmpList});
    };
    state = {
        show: true,
        components: [
            {type: 'warning', show: true, id: '1'},
            {type: 'primary', show: true, handler: this.someHandler, id: '2'},
            {type: 'success', show: true, handler: this.someHandler, id: '3'},
            {type: 'danger', show: true, handler: this.someHandler, id: '4'}
        ]
    };


    render() {
        return (
            this.state.components.map(function (element) {
                return (<Alert
                    type={element.type}
                    dismiss={(element.handler) ? () => element.handler(element.id) : null}
                    show={element.show}
                    key={element.id}

                >This is a warning type alert</Alert>);
            })
        );
    }
}

export default App;
